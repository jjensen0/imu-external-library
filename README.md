IMU External Library
======================

General
-------
The Inertial Measurement Unit (IMU) external library is to be used in conjunction with the CEENBoT API static library.  It allows the CEENBoT to communicate with an external motion sensor.  Currently, the two sensors supported by this library are the ADXL345 triple axis accelerometer and the MPU6050 six axis accelerometer and gyroscope.

License
---
Copyright (C) 2014 The Board of Regents of the University of Nebraska.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Conventions
------------
The IMU External Library follows the coding conventions of the C API written by Jose Santos. Observations are as follows:

*   All #DEFINEs must be capitalizing all letters (I.E: #DEFINE THIS_IS_MY_DEFINE 0)
*   All spaces that would typically be in a phrase are replaced with underscores. (I.E: this_is_my_function())
*   All public functions should capitalize the system name followed by lower case function name (I.E: LCD_open())
*   All private functions should have two underscores before the system name. (IE: __LCD_close())


First time
---------
The user will need to link to the library by indicating its path in the respective  program.  For example, if working in Eclipse, the following steps must be taken to include the external library.

1. You will need to specify the Include Path to the header files of the external library by taking the following steps: 
	a. Right click on the project folder in Eclipse and select Properties
	b. Under C/C++ Build, select Settings
	c. Under AVR Compiler, select Directories
	d. Add the Include Path (-l) by clicking the "+" button and then selecting the includes folder in the external library.  The path will be something like this:
	/Users/USERNAME/Desktop/imu-external-library/include/
	e. Note that the path to the CEENBoT API's lib-includes folder must also be included.  This path will look something like:
	/Users/USERNAME/Desktop/ceenbot-api/lib/lib-includes
	
2. You will need to include the library name and the path to the library by taking the following steps:
	a. In the same manner, right click on the project folder to get to Properties > C/C++ Build > Settings
	b. Under AVR C Linker, select Libraries
	c. Add the external library to the Libraries (-l) box by clicking the "+" button and typing "cbot_imu"
	d. Add the path to the external library in the Libraries Path (-L) by selecting the "+" button and finding the path to the folder on your computer containing the external library's .a file.  This should just be the main folder where the IMU External Library is stored.
	
There are other steps that must be taken to use Eclipse to program the CEENBoT.  These steps are specified in the document titled "CEENBoT Programming in C with Eclipse and WinAVR," which can be found on the following web page:

http://educationalrobotics.wikispaces.com/C+Language++Programming

The above web page also specifies how to use AVR Studio to programm the CEENBoT.


Credits
--------
The IMU External Library was created by Cannon Biggs and Trevor Hoke.