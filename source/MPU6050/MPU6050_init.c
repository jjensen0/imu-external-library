/*****************************************************************************************
 *	Copyright (C) 2014 The Board of Regents of the University of Nebraska.				 *
 *	All rights reserved.																 *
 *																						 *
 *	Redistribution and use in source and binary forms, with or without modification,	 *
 *	are permitted provided that the following conditions are met:						 *
 *																						 *
 *	1. Redistributions of source code must retain the above copyright notice, this list  *
 *		of conditions and the following disclaimer.										 *
 *																						 *
 *	2. Redistributions in binary form must reproduce the above copyright notice, this 	 *
 *		list of conditions and the following disclaimer in the documentation and/or 	 *
 *		other materials provided with the distribution.									 *
 *																						 *
 *	3. Neither the name of the copyright holder nor the names of its contributors may be *
 *		used to endorse or promote products derived from this software without specific  *
 *		prior written permission.														 *
 *																						 *
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY  *
 *	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES *
 *	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT  *
 *	SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 		 *
 *	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED *
 *	TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	 *
 *	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 	 *
 *	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN   *
 *	ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH  *
 *	DAMAGE.																				 *
 *																						 *
 *****************************************************************************************/

// Auth: Cannon Biggs
// Desc: Sets default configuration for device and tests connection with device.
//		 Returns an I2C_Status enumerated value that corresponds to the status
//		 of the I2C communication transaction.

/**
 * Power on and prepare for general usage.
 * This will activate the device and take it out of sleep mode (which must be done
 * after start-up). This function also sets both the accelerometer and the gyroscope
 * to their most sensitive settings, namely +/- 2g and +/- 250 degrees/sec, and sets
 * the clock source to use the X Gyro for reference, which is slightly better than
 * the default internal clock source.
 *
 ** On setting clock source setting:
 * An internal 8MHz oscillator, gyroscope based clock, or external sources can
 * be selected as the MPU-60X0 clock source. When the internal 8 MHz oscillator
 * or an external source is chosen as the clock source, the MPU-60X0 can operate
 * in low power modes with the gyroscopes disabled.
 *
 * Upon power up, the MPU-60X0 clock source defaults to the internal oscillator.
 * However, it is highly recommended that the device be configured to use one of
 * the gyroscopes (or an external clock source) as the clock reference for
 * improved stability. The clock source can be selected according to the following table:
 *
 * 
 * CLK_SEL | Clock Source
 * --------+--------------------------------------
 * 0       | Internal 8 MHz oscillator
 * 1       | PLL with X Gyro reference	 ( TODO: How does this work? )
 * 2       | PLL with Y Gyro reference
 * 3       | PLL with Z Gyro reference
 * 4       | PLL with external 32.768kHz reference
 * 5       | PLL with external 19.2MHz reference
 * 6       | Reserved
 * 7       | Stops the clock and keeps the timing generator in reset
 */

#include "cbot_imu.h"

I2C_STATUS MPU6050_init( void )
{
	uint8_t temp;

	// Assume everything went okay
	I2C_STATUS rval = I2C_STAT_OK;

	// Test connection with device
	rval = MPU6050_test();

	if ( rval == I2C_STAT_OK )
	{
		// Set clock source setting to Phase-Locked Loop with X axis gyroscope
		// as reference.
		temp = (0x00)|(1 << TEMP_DIS)|(CLKSEL_PLL_XGYRO);
		MPU6050_write( MPU6050_PWR_MGMT_1, temp );

		// set full-scale gyroscope range
		temp = (0x00)|(MPU6050_GYRO_FS_250 << MPU6050_GCONFIG_FS_SEL_BIT);
		MPU6050_write( MPU6050_GYRO_CONFIG, temp );

		// set full-scale accelerometer range
		temp = (0x00)|(MPU6050_ACCEL_FS_2 << MPU6050_ACONFIG_AFS_SEL_BIT);
		MPU6050_write( MPU6050_ACCEL_CONFIG, temp );

		// Set on-chip Low Pass Filter configuration to max. Will still receive
		// data at 1kHz (if using data ready interrupt)
		MPU6050_write( MPU6050_CONFIG, 0x06);
	}

	return rval;

} // end MPU6050_init()
