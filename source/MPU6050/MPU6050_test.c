/*****************************************************************************************
 *	Copyright (C) 2014 The Board of Regents of the University of Nebraska.				 *
 *	All rights reserved.																 *
 *																						 *
 *	Redistribution and use in source and binary forms, with or without modification,	 *
 *	are permitted provided that the following conditions are met:						 *
 *																						 *
 *	1. Redistributions of source code must retain the above copyright notice, this list  *
 *		of conditions and the following disclaimer.										 *
 *																						 *
 *	2. Redistributions in binary form must reproduce the above copyright notice, this 	 *
 *		list of conditions and the following disclaimer in the documentation and/or 	 *
 *		other materials provided with the distribution.									 *
 *																						 *
 *	3. Neither the name of the copyright holder nor the names of its contributors may be *
 *		used to endorse or promote products derived from this software without specific  *
 *		prior written permission.														 *
 *																						 *
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY  *
 *	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES *
 *	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT  *
 *	SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 		 *
 *	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED *
 *	TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	 *
 *	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 	 *
 *	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN   *
 *	ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH  *
 *	DAMAGE.																				 *
 *																						 *
 *****************************************************************************************/

// Auth: Cannon Biggs
// Desc: Implementation file for IMU_test function for MPU6050.
//		 It tests the connection with the device by calling its DEV_ID
//		 and compares the result it gets with the value it's supposed to be.
//		 Returns an IMU_STATUS enumerated value.
// 
// Full Description:
// The __MPU6050_test() function calls the WHO_AM_I register on the MPU-6050.
// Per the datasheet, the WHO_AM_I register can be used to verify the 
// identity of the device.  The contents of the WHO_AM_I are the upper 6
// bits of the MPU-6050's 7-bit I2C address.  The least significant bit of the
// MPU-6050's I2C address is determined b the value of the AD0 pin (whether
// it is tied to the voltage source or to ground).  However, the value of the
// AD0 pin is not reflected in the WHO_AM_I register.
//
// The default value of the register is 0x68.  If the device does not 
// return this value, something is off with the communication or we are
// using a different device.

#include "cbot_imu.h"


I2C_STATUS MPU6050_test( void )
{
	// Assume everything is okay
	I2C_STATUS rval = I2C_STAT_OK;

	uint8_t device_id;

	device_id = MPU6050_read( MPU6050_WHO_AM_I, 1 )[0];

	if ( device_id != MPU6050_ADDR )
	{
		rval = I2C_STAT_START_ERROR;
	}

	return rval;

} // end test
