/*****************************************************************************************
 *	Copyright (C) 2014 The Board of Regents of the University of Nebraska.				 *
 *	All rights reserved.																 *
 *																						 *
 *	Redistribution and use in source and binary forms, with or without modification,	 *
 *	are permitted provided that the following conditions are met:						 *
 *																						 *
 *	1. Redistributions of source code must retain the above copyright notice, this list  *
 *		of conditions and the following disclaimer.										 *
 *																						 *
 *	2. Redistributions in binary form must reproduce the above copyright notice, this 	 *
 *		list of conditions and the following disclaimer in the documentation and/or 	 *
 *		other materials provided with the distribution.									 *
 *																						 *
 *	3. Neither the name of the copyright holder nor the names of its contributors may be *
 *		used to endorse or promote products derived from this software without specific  *
 *		prior written permission.														 *
 *																						 *
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY  *
 *	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES *
 *	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT  *
 *	SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 		 *
 *	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED *
 *	TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	 *
 *	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 	 *
 *	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN   *
 *	ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH  *
 *	DAMAGE.																				 *
 *																						 *
 *****************************************************************************************/

// Auth: Cannon Biggs
// Desc: This function reads multiple bytes from the address specified on the MPU6050.
//		 The I2C function I2C_MSTR_get_multiple() is used to get multiple bytes of data.
//		 The programmer must specify the number of bytes to be received in the parameters
//		 of this function. The function returns the address to the buffer storing the data.

#include "cbot_imu.h"


int8_t* MPU6050_read( uint8_t address, unsigned short int count )
{
	unsigned char pBuffer[ count ];
	int8_t* rval;

	// Master Transmit --> transmit address where we will start reading from
	if ( I2C_MSTR_start( MPU6050_ADDR, I2C_MODE_MT ) == I2C_STAT_OK )
	{
		// Send address from which we'll start multi-byte read
		I2C_MSTR_send( address );
	}

	if ( I2C_MSTR_start( MPU6050_ADDR, I2C_MODE_MR ) == I2C_STAT_OK )
	{
		I2C_MSTR_get_multiple( pBuffer, count );
	}

	// end transaction
	I2C_MSTR_stop();

	rval = (int8_t*) pBuffer;
	return rval;

} // MPU6050_read()
