/*****************************************************************************************
 *	Copyright (C) 2014 The Board of Regents of the University of Nebraska.				 *
 *	All rights reserved.																 *
 *																						 *
 *	Redistribution and use in source and binary forms, with or without modification,	 *
 *	are permitted provided that the following conditions are met:						 *
 *																						 *
 *	1. Redistributions of source code must retain the above copyright notice, this list  *
 *		of conditions and the following disclaimer.										 *
 *																						 *
 *	2. Redistributions in binary form must reproduce the above copyright notice, this 	 *
 *		list of conditions and the following disclaimer in the documentation and/or 	 *
 *		other materials provided with the distribution.									 *
 *																						 *
 *	3. Neither the name of the copyright holder nor the names of its contributors may be *
 *		used to endorse or promote products derived from this software without specific  *
 *		prior written permission.														 *
 *																						 *
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY  *
 *	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES *
 *	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT  *
 *	SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 		 *
 *	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED *
 *	TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	 *
 *	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 	 *
 *	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN   *
 *	ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH  *
 *	DAMAGE.																				 *
 *																						 *
 *****************************************************************************************/

// Auth: Cannon Biggs
// Desc: Reads MPU-6050 6-axis motion data as a multi-byte read,
// 		 starting from register MPU6050_ACCEL_XOUT_H

#include "cbot_imu.h"


void MPU6050_get_data( MOTION_DATA *data )
{
	// Will need to read 14 bytes (starting at 1) to update MOTION_DATA
	// Note that we get the HIGH byte first from the MPU6050
	int8_t *_buffer = MPU6050_read( MPU6050_ACCEL_XOUT_H, 14 );

	data->accel.x = ((_buffer[0] << 8) | _buffer[1]);

	data->accel.y = ((_buffer[2] << 8) | _buffer[3]);

	data->accel.z = ((_buffer[4] << 8) | _buffer[5]);

	// The robot can return temperature as well, but currently we're not
	// implementing it.
//  temperature   = ((_buffer[6] << 8) | _buffer[7]);

	data->gyro.x  = ((_buffer[8] << 8) | _buffer[9] );

	data->gyro.y = ((_buffer[10] << 8) | _buffer[11] );

	data->gyro.z = ((_buffer[12] << 8) | _buffer[13] );

} // end MPU6050_get_data()
