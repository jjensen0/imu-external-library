/*****************************************************************************************
 *	Copyright (C) 2014 The Board of Regents of the University of Nebraska.				 *
 *	All rights reserved.																 *
 *																						 *
 *	Redistribution and use in source and binary forms, with or without modification,	 *
 *	are permitted provided that the following conditions are met:						 *
 *																						 *
 *	1. Redistributions of source code must retain the above copyright notice, this list  *
 *		of conditions and the following disclaimer.										 *
 *																						 *
 *	2. Redistributions in binary form must reproduce the above copyright notice, this 	 *
 *		list of conditions and the following disclaimer in the documentation and/or 	 *
 *		other materials provided with the distribution.									 *
 *																						 *
 *	3. Neither the name of the copyright holder nor the names of its contributors may be *
 *		used to endorse or promote products derived from this software without specific  *
 *		prior written permission.														 *
 *																						 *
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY  *
 *	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES *
 *	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT  *
 *	SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 		 *
 *	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED *
 *	TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	 *
 *	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 	 *
 *	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN   *
 *	ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH  *
 *	DAMAGE.																				 *
 *																						 *
 *****************************************************************************************/

// Auth: Cannon Biggs
// Desc: Implementation file for 'IMU_open()' function.

#include "cbot_imu.h"

IMU_STAT IMU_open( IMU_DEVICE dev )
{
	IMU_STAT rval;

	// Assume there is an error
	rval = IMU_CLOSED;

	// Open ONLY if the subsystem is currently closed
	
	if ( __IMU_get_state() == IMU_CLOSED )
	{

		// Both currently implemented IMU devices, the MPU6050 and the ADXL345
		// use I2C to communicated back to the CEENBoT.
		
		I2C_open();

		// Both currently implemented devices need I2C pull-ups
		// enabled.  Therefore, if they are currently disabled,
		// enable the I2C pull-up resistors.
		
		if (I2C_params.using_pullups == FALSE)
		{
		
			I2C_pullup_enable();
			
		}

		// Initialize the device, communicating with it via I2C
		// If the communication goes ok, assume device is ready to be used.
		
		if ( __IMU_init( dev ) == I2C_STAT_OK )
		{
		
			// Denote the IMU is open for business
			
			__IMU_set_state( IMU_OPEN );

		}

	} // end if()

	rval = __IMU_get_state();
	
	return rval;

} // end IMU_open()
