/*****************************************************************************************
 *	Copyright (C) 2014 The Board of Regents of the University of Nebraska.				 *
 *	All rights reserved.																 *
 *																						 *
 *	Redistribution and use in source and binary forms, with or without modification,	 *
 *	are permitted provided that the following conditions are met:						 *
 *																						 *
 *	1. Redistributions of source code must retain the above copyright notice, this list  *
 *		of conditions and the following disclaimer.										 *
 *																						 *
 *	2. Redistributions in binary form must reproduce the above copyright notice, this 	 *
 *		list of conditions and the following disclaimer in the documentation and/or 	 *
 *		other materials provided with the distribution.									 *
 *																						 *
 *	3. Neither the name of the copyright holder nor the names of its contributors may be *
 *		used to endorse or promote products derived from this software without specific  *
 *		prior written permission.														 *
 *																						 *
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY  *
 *	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES *
 *	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT  *
 *	SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 		 *
 *	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED *
 *	TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	 *
 *	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 	 *
 *	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN   *
 *	ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH  *
 *	DAMAGE.																				 *
 *																						 *
 *****************************************************************************************/

// Auth: Cannon Biggs
// Desc: IMU_read() redirects to device-specific data reading functions,
//		 allowing the user to read multiple bytes from either the ADXL345
//		 or the MPU6050.
// PARAMS:	IMU_DEVICE dev				: The device to read from; must be either MPU6050 or ADXL345
//			uint8_t address				: unsigned 8-bit integer of address on device
//			unsigned short int numBytes : unsigned short int for number of bytes to be read.
// RETURNS: A pointer to the buffer where the data is stored.
// Notes: The type unsigned short int was used for the numBytes in order to be consistent
//		  with the I2C read function in the CBoT API, which required an unsigned short int
//		  in its parameter specifying the number of bytes to be read.

#include "cbot_imu.h"

int8_t* IMU_read( IMU_DEVICE dev, uint8_t address, unsigned short int numBytes )
{
	int8_t *pBuffer = NULL;

	switch( dev ) {

	case MPU6050:
		pBuffer = MPU6050_read( address, numBytes );
		break;

	case ADXL345:
		pBuffer = ADXL345_read( address, numBytes );
		break;

	}

	return pBuffer;

} // end IMU_read()
