/*****************************************************************************************
 *	Copyright (C) 2014 The Board of Regents of the University of Nebraska.				 *
 *	All rights reserved.																 *
 *																						 *
 *	Redistribution and use in source and binary forms, with or without modification,	 *
 *	are permitted provided that the following conditions are met:						 *
 *																						 *
 *	1. Redistributions of source code must retain the above copyright notice, this list  *
 *		of conditions and the following disclaimer.										 *
 *																						 *
 *	2. Redistributions in binary form must reproduce the above copyright notice, this 	 *
 *		list of conditions and the following disclaimer in the documentation and/or 	 *
 *		other materials provided with the distribution.									 *
 *																						 *
 *	3. Neither the name of the copyright holder nor the names of its contributors may be *
 *		used to endorse or promote products derived from this software without specific  *
 *		prior written permission.														 *
 *																						 *
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY  *
 *	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES *
 *	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT  *
 *	SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 		 *
 *	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED *
 *	TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	 *
 *	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 	 *
 *	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN   *
 *	ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH  *
 *	DAMAGE.																				 *
 *																						 *
 *****************************************************************************************/

// Auth: Cannon Biggs & TJ Hoke
// Desc: ADXL345_configure is a function which sets a default setting for the registers.
//		 In other words, a don't care about bells and whistles just want accel data.
//		 
// Dependent on:
//		I2C functions (opened with IMU_open)
//		and the ADXL345_write module ,which sets registers.


#include "cbot_imu.h"

I2C_STATUS ADXL345_init( void )
{
	// Assume everything is okay
	I2C_STATUS rval = I2C_STAT_OK;

	rval = ADXL345_test();

	if ( rval == I2C_STAT_OK )
	{
		// Puts device in sleep mode
		ADXL345_write( ADXL345_POWER_CTRL,  0x00 );

		// & set D0 to put ADXL345 into +/- 16G range
		// TODO: look at range for consistency
		ADXL345_write( ADXL345_DATA_FORMAT, 0x0B );

		// Should set The system to low power, and 200Hz
		// there is a default on system about high power 100Hz
		ADXL345_write( ADXL345_BW_RATE,     0x1B );

		// Put ADXL345 into Measurement Mode by writing 0x08 to the POWER_CTRL reg.
		ADXL345_write( ADXL345_POWER_CTRL,  0x08 );

	}

	return rval;
	
} // end ADXL345_init()
