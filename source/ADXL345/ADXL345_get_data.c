/*****************************************************************************************
 *	Copyright (C) 2014 The Board of Regents of the University of Nebraska.				 *
 *	All rights reserved.																 *
 *																						 *
 *	Redistribution and use in source and binary forms, with or without modification,	 *
 *	are permitted provided that the following conditions are met:						 *
 *																						 *
 *	1. Redistributions of source code must retain the above copyright notice, this list  *
 *		of conditions and the following disclaimer.										 *
 *																						 *
 *	2. Redistributions in binary form must reproduce the above copyright notice, this 	 *
 *		list of conditions and the following disclaimer in the documentation and/or 	 *
 *		other materials provided with the distribution.									 *
 *																						 *
 *	3. Neither the name of the copyright holder nor the names of its contributors may be *
 *		used to endorse or promote products derived from this software without specific  *
 *		prior written permission.														 *
 *																						 *
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY  *
 *	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES *
 *	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT  *
 *	SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 		 *
 *	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED *
 *	TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	 *
 *	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 	 *
 *	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN   *
 *	ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH  *
 *	DAMAGE.																				 *
 *																						 *
 *****************************************************************************************/

// Auth: Cannon Biggs & TJ Hoke
// Desc: ADXL345 get data, this gets the data for the axes. The actual work is done by 
//		 ADXL345_read.  Data is stored directly in the struct, so nothing needs to be returned
// 		 by the function.
//
//	   Dependent on:
//		 I2C functions (opened with IMU_open)
//		 ADXL345_read

#include "cbot_imu.h"

void ADXL345_get_data( MOTION_DATA *data )
{
	int8_t *_buffer = ADXL345_read( ADXL345_DATAX0, 6 );

	// ADXL345 reads the low byte first
	data->accel.x = ((int)_buffer[1]<< 8 | (int)_buffer[0]);
	data->accel.y = ((int)_buffer[3]<< 8 | (int)_buffer[2]);
	data->accel.z = ((int)_buffer[5]<< 8 | (int)_buffer[4]);
	
} // end ADXL345_get_data()
