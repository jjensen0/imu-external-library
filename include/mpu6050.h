/*****************************************************************************************
 *	Copyright (C) 2014 The Board of Regents of the University of Nebraska.				 *
 *	All rights reserved.																 *
 *																						 *
 *	Redistribution and use in source and binary forms, with or without modification,	 *
 *	are permitted provided that the following conditions are met:						 *
 *																						 *
 *	1. Redistributions of source code must retain the above copyright notice, this list  *
 *		of conditions and the following disclaimer.										 *
 *																						 *
 *	2. Redistributions in binary form must reproduce the above copyright notice, this 	 *
 *		list of conditions and the following disclaimer in the documentation and/or 	 *
 *		other materials provided with the distribution.									 *
 *																						 *
 *	3. Neither the name of the copyright holder nor the names of its contributors may be *
 *		used to endorse or promote products derived from this software without specific  *
 *		prior written permission.														 *
 *																						 *
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY  *
 *	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES *
 *	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT  *
 *	SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 		 *
 *	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED *
 *	TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	 *
 *	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 	 *
 *	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN   *
 *	ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH  *
 *	DAMAGE.																				 *
 *																						 *
 *****************************************************************************************/

// Auth: Cannon Biggs
// Desc: This is the header file for the MPU6050 six axis accelerometer and gyroscope.

/** Accelerometer Specifications: **
 *
 * __PARAMETER__				__CONDITIONS__		__MIN__		__TYP__		__MAX__		__UNITS__
 * ACCELEROMETER SENSITIVITY
 * Full-Scale Range				AFS_SEL = 0						+/- 2						g
 * 								AFS_SEL = 1						+/- 4						g
 * 								AFS_SEL = 2						+/- 8						g
 * 								AFS_SEL = 3						+/- 16						g
 *
 * ADC Word Length				Output in 2's					  16			   		   bits
 * 							  	complement format
 *
 * Sensitivity Scale Factor		AFS_SEL = 0						16384					  LSB/g
 * 								AFS_SEL = 1						8192					  LSB/g
 * 								AFS_SEL = 2						4096					  LSB/g
 * 								AFS_SEL = 3						2048					  LSB/g
 *
 * LOW PASS FILTER RESPONSE		Programmable Range	   5					  260		   Hz
 * OUTPUT DATA RATE				Programmable Range	   4					  1,000		   Hz
 */

/** Gyroscope Specifications: **
 *
 * __PARAMETER__				__CONDITIONS__		__MIN__		__TYP__		__MAX__		__UNITS__
 * ACCELEROMETER SENSITIVITY
 * Full-Scale Range				FS_SEL = 0						+/- 250					   ¬∞/s
 * 								FS_SEL = 1						+/- 500					   ¬∞/s
 * 								FS_SEL = 2						+/- 1000				   ¬∞/s
 * 								FS_SEL = 3						+/- 2000				   ¬∞/s
 *
 * ADC Word Length				Output in 2's					  16			   		   bits
 * 							  	complement format
 *
 * Sensitivity Scale Factor		FS_SEL = 0						 131					  LSB/g
 * 								FS_SEL = 1						 65.5					  LSB/g
 * 								FS_SEL = 2						 32.8					  LSB/g
 * 								FS_SEL = 3						 16.4					  LSB/g
 *
 * LOW PASS FILTER RESPONSE		Programmable Range	   5					  256		   Hz
 * OUTPUT DATA RATE				Programmable Range	   4					  8,000		   Hz
 */



#ifndef __MPU6050_H__
#define __MPU6050_H__

#include "cbot_imu.h"

// =============================== defines ================================== //

#define MPU6050_ADDRESS_AD0_LOW		0x68
#define MPU6050_ADDRESS_AD0_HIGH	0x69

// set default configuration
#define MPU6050_ADDR	MPU6050_ADDRESS_AD0_LOW

// MPU6050 register addresses & bits
#define MPU6050_XG_OFFS_TC       	0x00 //[7] PWR_MODE, [6:1] XG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_YG_OFFS_TC       	0x01 //[7] PWR_MODE, [6:1] YG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_ZG_OFFS_TC       	0x02 //[7] PWR_MODE, [6:1] ZG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_X_FINE_GAIN      	0x03 //[7:0] X_FINE_GAIN
#define MPU6050_Y_FINE_GAIN      	0x04 //[7:0] Y_FINE_GAIN
#define MPU6050_Z_FINE_GAIN      	0x05 //[7:0] Z_FINE_GAIN
#define MPU6050_XA_OFFS_H        	0x06 //[15:0] XA_OFFS
#define MPU6050_XA_OFFS_L_TC     	0x07
#define MPU6050_YA_OFFS_H        	0x08 //[15:0] YA_OFFS
#define MPU6050_YA_OFFS_L_TC     	0x09
#define MPU6050_ZA_OFFS_H        	0x0A //[15:0] ZA_OFFS
#define MPU6050_ZA_OFFS_L_TC     	0x0B
#define MPU6050_XG_OFFS_USRH     	0x13 //[15:0] XG_OFFS_USR
#define MPU6050_XG_OFFS_USRL     	0x14
#define MPU6050_YG_OFFS_USRH     	0x15 //[15:0] YG_OFFS_USR
#define MPU6050_YG_OFFS_USRL     	0x16
#define MPU6050_ZG_OFFS_USRH     	0x17 //[15:0] ZG_OFFS_USR
#define MPU6050_ZG_OFFS_USRL     	0x18
#define MPU6050_SMPLRT_DIV       	0x19
#define MPU6050_CONFIG           	0x1A
#define MPU6050_GYRO_CONFIG      	0x1B
#define MPU6050_ACCEL_CONFIG     	0x1C
#define MPU6050_FF_THR           	0x1D
#define MPU6050_FF_DUR           	0x1E
#define MPU6050_MOT_THR          	0x1F
#define MPU6050_MOT_DUR          	0x20
#define MPU6050_ZRMOT_THR        	0x21
#define MPU6050_ZRMOT_DUR        	0x22
#define MPU6050_FIFO_EN          	0x23
#define MPU6050_ACCEL_XOUT_H     	0x3B
#define MPU6050_ACCEL_XOUT_L     	0x3C
#define MPU6050_ACCEL_YOUT_H     	0x3D
#define MPU6050_ACCEL_YOUT_L     	0x3E
#define MPU6050_ACCEL_ZOUT_H     	0x3F
#define MPU6050_ACCEL_ZOUT_L     	0x40
#define MPU6050_TEMP_OUT_H       	0x41
#define MPU6050_TEMP_OUT_L       	0x42
#define MPU6050_GYRO_XOUT_H      	0x43
#define MPU6050_GYRO_XOUT_L      	0x44
#define MPU6050_GYRO_YOUT_H      	0x45
#define MPU6050_GYRO_YOUT_L      	0x46
#define MPU6050_GYRO_ZOUT_H      	0x47
#define MPU6050_GYRO_ZOUT_L      	0x48
#define MPU6050_MOT_DETECT_STATUS   0x61
#define MPU6050_SIGNAL_PATH_RESET   0x68		// what is this?
#define MPU6050_MOT_DETECT_CTRL     0x69
#define MPU6050_USER_CTRL       	0x6A
#define MPU6050_PWR_MGMT_1			0x6B
#define	MPU6050_PWR_MGMT_2			0x6C
#define MPU6050_BANK_SEL         	0x6D
#define MPU6050_MEM_START_ADDR   	0x6E
#define MPU6050_MEM_R_W          	0x6F
#define MPU6050_DMP_CFG_1        	0x70
#define MPU6050_DMP_CFG_2        	0x71
#define MPU6050_FIFO_COUNTH      	0x72
#define MPU6050_FIFO_COUNTL      	0x73
#define MPU6050_FIFO_R_W         	0x74
#define MPU6050_WHO_AM_I			0x75

#define MPU6050_GCONFIG_FS_SEL_BIT      4
#define MPU6050_GCONFIG_FS_SEL_LENGTH   2

#define MPU6050_GYRO_FS_250         0x00
#define MPU6050_GYRO_FS_500         0x01
#define MPU6050_GYRO_FS_1000        0x02
#define MPU6050_GYRO_FS_2000        0x03

#define MPU6050_ACONFIG_XA_ST_BIT           7
#define MPU6050_ACONFIG_YA_ST_BIT           6
#define MPU6050_ACONFIG_ZA_ST_BIT           5
#define MPU6050_ACONFIG_AFS_SEL_BIT         4
#define MPU6050_ACONFIG_AFS_SEL_LENGTH      2
#define MPU6050_ACONFIG_ACCEL_HPF_BIT       2
#define MPU6050_ACONFIG_ACCEL_HPF_LENGTH    3

#define MPU6050_ACCEL_FS_2          0x00
#define MPU6050_ACCEL_FS_4          0x01
#define MPU6050_ACCEL_FS_8          0x02
#define MPU6050_ACCEL_FS_16         0x03

// PWR_MGMT_1 bits
#define DEVICE_RESET   7
#define SLEEP          6
#define CYCLE          5
//		-			   4
#define TEMP_DIS       3
#define CLKSEL         2

// CLKSEL bits in PWR_MGMT_1 register
// 	For further infor on clock source, see Product Specs document
#define CLKSEL_INT8M          0		// Internal 8MHz oscillator
#define CLKSEL_PLL_XGYRO      1
#define CLKSEL_PLL_YGYRO      2
#define CLKSEL_PLL_ZGYRO      3
#define CLKSEL_PLL_EXT32K     4
#define CLKSEL_PLL_EXT19M     5
#define CLKSEL_KEEP_RESET     7		// Stops the clock & keeps the timing
									// generator in reset

#define AccelScale2 	16384
#define GyroScale250 	131
#define GyroScale 		GyroScale250
#define AccelScale 		AccelScale2


// =============================== prototypes =============================== //
// Desc: MPU6050_init() initializes the MPU6050 device to its default
// 		 configurations and tests connection with device.  It returns an
//		 enumerated value that represents the status of the I2C
//		 communication transaction.
extern I2C_STATUS MPU6050_init( void );
// -------------------------------------------------------------------------- //
// Desc: MPU6050_test() is inherently called in MPU6050_init(), in order to
//		 test the connection between the MPU6050 and the CEENBoT.  Returns
//		 an enum value that represents status of I2C transaction.
extern I2C_STATUS MPU6050_test( void );
// -------------------------------------------------------------------------- //
// Desc: The MPU6050_get_data() receives accelerometer and gyroscope data
//		 at the same time from the MPU6050 device and stores the data in
//		 a MOTION_DATA struct.
//		 This is defined in the cbot_imu.h file due to unknown errors
//		 encountered in eclipse at the time of implementation.
//extern void MPU6050_get_data( MOTION_DATA *data );
// -------------------------------------------------------------------------- //
// Desc: MPU6050_read() reads a user-specified number of bytes of data starting
//		 from a user-specified address on the MPU-6050.  The parameters of the
//		 fucntion are a unsigned char for the address and an unsigned short int
//		 for the count of bytes to be received.  The count was made to be
//		 an unsigned short int in order to be consistent with the CBOT API I2C
//		 fucntion for a multi-byte read.
//		 MPU_6050() returns a pointer to the buffer where the data is stored.
extern int8_t* MPU6050_read( uint8_t address, unsigned short int count );
// -------------------------------------------------------------------------- //
// Desc: MPU6050_write() writes a single byte to an address on the MPU6050.
//
extern void MPU6050_write( uint8_t address, uint8_t value );
// -------------------------------------------------------------------------- //


#endif
