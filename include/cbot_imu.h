/*****************************************************************************************
 *	Copyright (C) 2014 The Board of Regents of the University of Nebraska.				 *
 *	All rights reserved.																 *
 *																						 *
 *	Redistribution and use in source and binary forms, with or without modification,	 *
 *	are permitted provided that the following conditions are met:						 *
 *																						 *
 *	1. Redistributions of source code must retain the above copyright notice, this list  *
 *		of conditions and the following disclaimer.										 *
 *																						 *
 *	2. Redistributions in binary form must reproduce the above copyright notice, this 	 *
 *		list of conditions and the following disclaimer in the documentation and/or 	 *
 *		other materials provided with the distribution.									 *
 *																						 *
 *	3. Neither the name of the copyright holder nor the names of its contributors may be *
 *		used to endorse or promote products derived from this software without specific  *
 *		prior written permission.														 *
 *																						 *
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY  *
 *	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES *
 *	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT  *
 *	SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 		 *
 *	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED *
 *	TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	 *
 *	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 	 *
 *	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN   *
 *	ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH  *
 *	DAMAGE.																				 *
 *																						 *
 *****************************************************************************************/

// Auth: Cannon Biggs & TJ Hoke
// Desc: This header file is specific to the IMU, or Inertial Measurement Unit
// 		 subsystem module. An IMU is a device that measures inertia, which
//		 describes the motion of an object and how it is affected by externally
//		 applied forces.  An IMU can include an accelerometer, gyroscope,
//		 magnetometer, among others.
//
//		 Currently, the API supports the following two external IMU
//		 sensor boards:
//		 	- ADXL345 by Analog Devices and
//			- MPU-6050 by InvenSense

#ifndef __CBOT_IMU_H__
#define __CBOT_IMU_H__

/* Include header files that are used by both IMU modules */
#include "capi324v221.h"
#include "adxl345.h"
#include "mpu6050.h"

// ====================== global type declarations ========================== //
typedef enum IMU_ST_TYPE
{

    IMU_CLOSED = 0,
    IMU_OPEN,
    IMU_IN_USE,
    IMU_NOT_AVAILBLE,
    IMU_ERROR,
    IMU_INIT_FAILED

} IMU_STAT;

volatile IMU_STAT IMU_state;     // Holds state of IMU system


typedef enum IMU_DEVICE_TYPE {

	MPU6050,
	ADXL345

} IMU_DEVICE;


typedef struct IMU_MOTION_DATA {

	struct {

		int x;
		int y;
		int z;

	} accel;

	struct {

		int x;
		int y;
		int z;

	} gyro;

} MOTION_DATA;


#define IMU_config()	IMU_write()

// ============================ prototypes ================================== //

// Desc: Opens the IMU subsystem and returns a SUBSYS_OPENSTAT enumerated value.
extern IMU_STAT IMU_open( IMU_DEVICE device );
// -------------------------------------------------------------------------- //
// Desc: Closes the IMU subsystem module.
extern IMU_STAT IMU_close( IMU_DEVICE device );
// -------------------------------------------------------------------------- //
// Desc: Initializes specific device to default configuration, tests the
// communication with the device, and if it communicates okay, sends back
// and I2C_STATUS of I2C_STAT_OK, signaling that device is ready to use.
extern I2C_STATUS __IMU_init( IMU_DEVICE dev );
// -------------------------------------------------------------------------- //
// Desc: Redirects to device-specific data reading functions, such that the
//		 user can read accelerometer data from the ADXL345 and both
//		 accelerometer and gyroscope data from the MPU-6050, depending on
//		 which device is specified by the user.  Returns a MOTION_DATA struct.
extern MOTION_DATA IMU_get_data( IMU_DEVICE device );
// -------------------------------------------------------------------------- //
// Desc: Redirects to device-specific reading functions.  User must specify
//		 in the parameters the device to be read from - must be either ADXL345
//		 or MPU-6050, an unsigned 8 bit value for the address to be read from
//		 and an unsigned short int specifying the number of bytes to be read.
//		 Returns a pointer to a buffer storing the data.
extern int8_t* IMU_read( IMU_DEVICE device, uint8_t address,
						 unsigned short int count );
// -------------------------------------------------------------------------- //
// Desc: Redirects to device-specific write functions.  User must specify in
//		 the parameters the device, an unsigned 8 bit integer for the address
//		 and an unsigned 8 bit integer for the value to be written.
extern void IMU_write( IMU_DEVICE device, uint8_t registerAddress,
					   uint8_t value );
// -------------------------------------------------------------------------- //
// Desc: This is a private function that gets the state of the IMU external
//		 module.  This function was created to follow the convention of
//		 the CEENBoT API, although it may not be necessary here.  Returns an
//		 enumerated value for the status of the IMU external sensor.
extern IMU_STAT __IMU_get_state( void );
// -------------------------------------------------------------------------- //
// Desc: This is a private function that sets the state of the IMU external
//		 module.  This function was created to follow the convention of
//		 of the CEENBoT API, in which the state of the subsystem modules are
//		 monitored, but this might not be necessary here.
extern void __IMU_set_state( IMU_STAT imu_stat );
// -------------------------------------------------------------------------- //
// Desc: The ADXL345_get_data() reads accelerometer data from the ADXL345.  It
//		 is called by the IMU_get_data() function.  There were unknown errors
//		 in Eclipse when compiling the library when this function prototype
//		 was placed in the ADXL345.h header file.  Placing the prototype in here
//		 got rid of the errors.
extern void ADXL345_get_data( MOTION_DATA *data );
// -------------------------------------------------------------------------- //
// Desc: The MPU6050_get_data() reads accelerometer and gyroscope data from
//		 the MPU-6050.  It is called by the IMU_get_data() function.  There
//		 were unknown errors in Eclipse when compiling the library when
// 		 this function prototype was placed in the ADXL345.h header file.
// 	 	 Placing the prototype in here got rid of the errors.
extern void MPU6050_get_data( MOTION_DATA *data );


#endif /* __CBOT_IMU_H__ */
