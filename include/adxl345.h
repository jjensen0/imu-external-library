/*****************************************************************************************
 *	Copyright (C) 2014 The Board of Regents of the University of Nebraska.				 *
 *	All rights reserved.																 *
 *																						 *
 *	Redistribution and use in source and binary forms, with or without modification,	 *
 *	are permitted provided that the following conditions are met:						 *
 *																						 *
 *	1. Redistributions of source code must retain the above copyright notice, this list  *
 *		of conditions and the following disclaimer.										 *
 *																						 *
 *	2. Redistributions in binary form must reproduce the above copyright notice, this 	 *
 *		list of conditions and the following disclaimer in the documentation and/or 	 *
 *		other materials provided with the distribution.									 *
 *																						 *
 *	3. Neither the name of the copyright holder nor the names of its contributors may be *
 *		used to endorse or promote products derived from this software without specific  *
 *		prior written permission.														 *
 *																						 *
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY  *
 *	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES *
 *	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT  *
 *	SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 		 *
 *	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED *
 *	TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	 *
 *	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 	 *
 *	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN   *
 *	ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH  *
 *	DAMAGE.																				 *
 *																						 *
 *****************************************************************************************/

// Auth: Cannon Biggs & TJ Hoke
// Desc: This is the header file specific to the ADXL345.
//		 It will define registers for ADXL345 and the specific functions for user.
//		 if you are looking at this to test useful information is.

/****************************************************
 *	pin connector, guide from chip to CEENbot Board	 *
 *													 *
 *	chip |  20pin |pin lable						 *
 *	-----+--------+-----------						 *
 *	GND .....20.......GND							 *
 *	VCC .....18.......3.3V							 *
 *	CS  .....18.......3.3V							 *
 *	INT1.....NA.......NA							 *
 * 	INT2.....NA.......NA							 *
 *	SDO .....20.......GND							 *
 *	SDA .....13.......MEGA 324 SDA					 *
 *	SCL .....12.......MEGA 324 SCL					 *
 *****************************************************/

#ifndef __ADXL345_H__
#define __ADXL345_H__

#include "cbot_imu.h"

// ============================= defines ==================================== //
#define ADXL345_WHO_AM_I			0x00	// register location for ID

/* With ALT ADDRESS (SDO) tied to ground, device address is 0x53 */
#define ADXL345_ADDR 				0x53	// I2C data righting ADDR

//chip settings
#define ADXL345_BW_RATE				0x2C	// Data rate receiving
#define ADXL345_POWER_CTRL          0x2D    // Power-saving features control
#define ADXL345_DATA_FORMAT         0x31    // Data format control

// data
#define ADXL345_DATAX0              0x32    // X-axis data 0
#define ADXL345_DATAX1              0x33    // X-axis data 1
#define ADXL345_DATAY0              0x34    // Y-axis data 0
#define ADXL345_DATAY1              0x35    // Y-axis data 1
#define ADXL345_DATAZ0              0x36    // Z-axis data 0
#define ADXL345_DATAZ1              0x37    // Z-axis data 1


// ============================ prototypes ================================== //
// Desc: Acts as a default configuration for ADXL345 registers.
extern I2C_STATUS ADXL345_init( void );
// -------------------------------------------------------------------------- //
// Desc: This is to test if chip is the chip.
extern I2C_STATUS ADXL345_test( void );
// -------------------------------------------------------------------------- //
// Desc: This is for collection of axis data all at once, uses structs to
//		 contain information.  This is defined in the cbot_imu.h file due to
// 		 errors in Eclipse.
//extern void ADXL345_get_data( MOTION_DATA *data );

// -------------------------------------------------------------------------- //
// Desc: Reads register contents for data, mostly used to collect axis data.
//		 This is private.
extern int8_t* ADXL345_read( unsigned char registerAddress, unsigned short int count );
// -------------------------------------------------------------------------- //
// Desc: Writes to specific registers. This is left open for curious students
//		 who want to play with data sheets.
extern void ADXL345_write( uint8_t registerAddress, uint8_t value );
// -------------------------------------------------------------------------- //


#endif
